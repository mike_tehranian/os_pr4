#include "magickminify.h"
#include "minifyjpeg.h"

/* Server-side function implementations */

bool_t minify_image_1_svc(image source, image *destination, struct svc_req *req) {
    ssize_t dst_len;

    magickminify_init();

    destination->content.content_val = magickminify(source.content.content_val, source.content.content_len, &dst_len);
    destination->content.content_len = dst_len;

    return 1;
}

int minify_prog_1_freeresult(SVCXPRT *transp, xdrproc_t xdr_result, caddr_t result) {
    magickminify_cleanup();
    xdr_free(xdr_result, result);

    return 1;
}
