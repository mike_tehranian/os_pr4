#include "minifyjpeg.h"
#include <stdio.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <string.h>
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <getopt.h>

#include "magickminify.h"
#include "steque.h"

#ifndef SIG_PF
#define SIG_PF void(*)(int)
#endif

#define USAGE                                                                 \
"usage:\n"                                                                    \
"  minifyjpeg_svc [options]\n"                                                 \
"options:\n"                                                                  \
"  -t [nthreads]       Number of threads (Default: 1)\n"                      \
"  -h                  Show this help message.\n"                             \

/* OPTIONS DESCRIPTOR ====================================================== */
static struct option gLongOptions[] = {
    {"nthreads",      required_argument,      NULL,           't'},
    {"help",          no_argument,            NULL,           'h'},
    {NULL,            0,                      NULL,             0}
};

typedef union {
    image minify_image_1_arg;
} minify_1_union_argument;

typedef struct {
    struct svc_req *rqstp;
    SVCXPRT transp;
    xdrproc_t _xdr_argument;
    xdrproc_t _xdr_result;
    bool_t (*local)(char *, void *, struct svc_req *);
    minify_1_union_argument argument;
} minify_1_request_args;

int _minify_image_1(image  *argp, void *result, struct svc_req *rqstp) {
    return (minify_image_1_svc(*argp, result, rqstp));
}

// Thread safe queue global variables
pthread_mutex_t mutex_request_queue;
steque_t *request_queue;
pthread_cond_t cv_incoming_request;
pthread_t *threads;

void minify_prog_1(minify_1_request_args* request_args) {
    union {
        image minify_image_1_res;
    } result;
    bool_t retval;

    retval = (bool_t) (*request_args->local)((char *)&request_args->argument, (void *)&result, request_args->rqstp);
    if (retval > 0 && !svc_sendreply(&request_args->transp, (xdrproc_t) request_args->_xdr_result, (char *)&result)) {
        svcerr_systemerr(&request_args->transp);
    }
    if (!svc_freeargs(&request_args->transp, (xdrproc_t) request_args->_xdr_argument, (caddr_t) &request_args->argument)) {
        fprintf(stderr, "%s", "unable to free arguments");
        free(request_args);
        exit(1);
    }
    if (!minify_prog_1_freeresult(&request_args->transp, request_args->_xdr_result, (caddr_t) &result)) {
        fprintf(stderr, "%s", "unable to free results");
    }

    free(request_args);
}

void* process_request(void *arg) {
    // Worker threads continuously loop and process each request in the queue
    for (;;) {
        pthread_mutex_lock(&mutex_request_queue);

        while (steque_isempty(request_queue)) {
            pthread_cond_wait(&cv_incoming_request, &mutex_request_queue);
        }

        minify_1_request_args *new_request;
        new_request = steque_pop(request_queue);
        pthread_mutex_unlock(&mutex_request_queue);
        minify_prog_1(new_request);
    }

    pthread_exit(NULL);
}

void initialize_worker_pool(int nthreads) {
    // Mutex for adding and removing requests
    pthread_mutex_init(&mutex_request_queue, NULL);

    // Conditional variable for workers waiting for new requests
    pthread_cond_init(&cv_incoming_request, NULL);

    // Thread-safe queue for requests
    request_queue = calloc(1, sizeof(steque_t));
    steque_init(request_queue);

    threads = calloc(nthreads, sizeof(pthread_t));
    // Thread pool of workers
    for (int i = 0; i < nthreads; i++) {
        pthread_create(&threads[i], NULL, process_request, NULL);
    }
}

void enqueue_request(minify_1_request_args *request_args) {
    pthread_mutex_lock(&mutex_request_queue);

    steque_enqueue(request_queue, request_args);
    pthread_cond_broadcast(&cv_incoming_request);

    pthread_mutex_unlock(&mutex_request_queue);
}

static void mt_request_handler(struct svc_req *rqstp, register SVCXPRT *transp) {
    minify_1_request_args *request_args;
    request_args = calloc(1, sizeof(minify_1_request_args));

    switch (rqstp->rq_proc) {
        case NULLPROC:
            (void) svc_sendreply(transp, (xdrproc_t) xdr_void, (char *)NULL);
            free(request_args);
            return;
        case MINIFY_IMAGE:
            request_args->_xdr_argument = (xdrproc_t) xdr_image;
            request_args->_xdr_result = (xdrproc_t) xdr_image;
            request_args->local = (bool_t (*) (char *, void *,  struct svc_req *))_minify_image_1;
            break;
        default:
            svcerr_noproc(transp);
            free(request_args);
            return;
    }

    // Initialize and copy over arguments needed to process request
    request_args->rqstp = rqstp;
    memcpy(&request_args->transp, transp, sizeof(SVCXPRT));
    memset((char *)&request_args->argument, 0, sizeof(request_args->argument));
    if (!svc_getargs(transp, (xdrproc_t) request_args->_xdr_argument, (caddr_t) &request_args->argument)) {
            svcerr_decode(transp);
            free(request_args);
            return;
    }

    enqueue_request(request_args);
}

void cleanup_worker_pool() {
    steque_destroy(request_queue);
    free(request_queue);
    free(threads);
}

int main(int argc, char **argv) {
    register SVCXPRT *transp;

    int option_char = 0;
    int nthreads = 1;
    // Parse and set command line arguments
    while ((option_char = getopt_long(argc, argv, "t:hx", gLongOptions, NULL)) != -1) {
        switch (option_char) {
            case 't': // nthreads
                nthreads = atoi(optarg);
                break;
            case 'h': // help
                fprintf(stdout, "%s", USAGE);
                exit(0);
                break;
            default:
                fprintf(stderr, "%s", USAGE);
                exit(1);
        }
    }

    /* not useful, but it ensures the initial code builds without warnings */
    if (nthreads < 1) {
        nthreads = 1;
    }

    initialize_worker_pool(nthreads);

    pmap_unset(MINIFY_PROG, MINIFY_VERS);

    transp = svcudp_create(RPC_ANYSOCK);
    if (transp == NULL) {
                fprintf(stderr, "%s", "cannot create udp service.");
                exit(1);
    }
    if (!svc_register(transp, MINIFY_PROG, MINIFY_VERS, mt_request_handler, IPPROTO_UDP)) {
                fprintf(stderr, "%s", "unable to register (MINIFY_PROG, MINIFY_VERS, udp).");
                exit(1);
    }

    transp = svctcp_create(RPC_ANYSOCK, 0, 0);
    if (transp == NULL) {
                fprintf(stderr, "%s", "cannot create tcp service.");
                exit(1);
    }
    if (!svc_register(transp, MINIFY_PROG, MINIFY_VERS, mt_request_handler, IPPROTO_TCP)) {
                fprintf(stderr, "%s", "unable to register (MINIFY_PROG, MINIFY_VERS, tcp).");
                exit(1);
    }

    // Initialize once from the main thread
    magickminify_init();

    svc_run();
    fprintf(stderr, "%s", "svc_run returned");

    // Clean up once from the main thread
    magickminify_cleanup();
    cleanup_worker_pool();
    exit(1);
}
