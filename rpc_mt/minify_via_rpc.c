#include "minifyjpeg_clnt.c"
#include "minifyjpeg_xdr.c"
#include <stdio.h>
#include <stdlib.h>

CLIENT* get_minify_client(char *server) {
    CLIENT *cl;
    // Timeout specified in seconds and microseconds
    struct timeval timeout = {5, 0};

    cl = clnt_create(server, MINIFY_PROG, MINIFY_VERS, "tcp");
    if (cl == NULL) {
        // Could not create connection with server
        // Print error and exit
        clnt_pcreateerror(server);
        exit(EXIT_FAILURE);
    }

    clnt_control(cl, CLSET_TIMEOUT, (char *) &timeout);

    return cl;
}

/*
The size of the minified file is not known before the call to the library that minimizes it,
therefore this function should allocate the right amount of memory to hold the minimized file
and return it in the last parameter to it
*/
int minify_via_rpc(CLIENT *cl, void* src_val, size_t src_len, size_t *dst_len, void **dst_val) {
    image src_image, dst_image;
    src_image.content.content_len = src_len;
    src_image.content.content_val = src_val;

    dst_image.content.content_val = malloc(src_len);

    enum clnt_stat response_code = minify_image_1(src_image, &dst_image, cl);
    if (response_code == RPC_TIMEDOUT) {
        clnt_perrno(response_code);
        clnt_perror(cl, "Timeout RPC to Server");
        return RPC_TIMEDOUT;
    }
    if (response_code != RPC_SUCCESS) {
        clnt_perrno(response_code);
        clnt_perror(cl, "Error with RPC to Server");
        return -1;
    }

    *dst_len = dst_image.content.content_len;
    *dst_val = dst_image.content.content_val;

    return 0;
}
